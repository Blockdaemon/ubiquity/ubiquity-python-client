import json
import socket


def read_file(path):
    try:
        fd = open(path)
        return fd
    except FileNotFoundError as e:
        raise FileNotFoundError(f"File '{e.filename}' not found, make sure you run tests from the root directory.")


def get_protocols():
    protocols_json_file = read_file("./test/protocols.json")
    protocols = json.load(protocols_json_file)

    protocols_json_file.close()
    return protocols


def get_open_port():
    s = socket.socket()
    s.bind(('', 0))
    return s.getsockname()[1]
