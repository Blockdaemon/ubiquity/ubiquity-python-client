import json
import os.path
import httpretty

MOCK_FILE_DIR = 'mock_files'


def get_mock_file_content(path):
    """
        Gets the content of file located under path
                relative to the test/mock_files directory
    """
    cur_dir = os.path.abspath(os.path.dirname(__file__))

    file = open(os.path.join(cur_dir, MOCK_FILE_DIR, path))
    return file.read()


def setup_mock_server(base_url, endpoints):
    """
        Setup httpretty mock server
    """
    for endpoint_data in endpoints:
        req_url = base_url + endpoint_data["req_url"]
        httpretty.register_uri(endpoint_data["method"],
                               req_url,
                               status=endpoint_data["status"],
                               body=endpoint_data["response_data"])


def is_protocol_supported(protocol, endpoint):
    """
        Gets mock about protocol information and returns True
        if the endpoint is supported by it according to the info
    """
    protocol_data_mock_file = get_mock_file_content(
        f"protocols_api/protocol_{protocol}.json")
    protocol_json_mock_data = json.loads(protocol_data_mock_file)
    return endpoint in protocol_json_mock_data["endpoints"]


def get_supported_protocols(protocols, endpoint):
    """
        Gets protocols that support 'endpoint' according to mocked
            response obtained by 'is_protocol_supported'
    """
    return [
        p for p in protocols if is_protocol_supported(p, endpoint)
    ]
