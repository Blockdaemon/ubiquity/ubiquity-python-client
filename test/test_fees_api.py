"""
    Ubiquity REST API

    Ubiquity provides a RESTful and uniform way to access blockchain resources, with a rich and reusable model across multiple cryptocurrencies.  [Documentation](https://app.blockdaemon.com/docs/ubiquity)  ### Protocols #### Mainnet The following protocols are currently supported: * bitcoin * ethereum * polkadot * xrp * algorand * stellar  #### Testnet Testnet support coming soon  ##### Pagination Certain resources contain a lot of data, more than what's practical to return for a single request. With the help of pagination, the data is split across multiple responses. Each response returns a subset of the items requested and a continuation token.  To get the next batch of items, copy the returned continuation token to the continuation query parameter and repeat the request with the new URL. In case no continuation token is returned, there is no more data available.   # noqa: E501

    The version of the OpenAPI document: 2.0.0
    Contact: support@blockdaemon.com
    Generated by: https://openapi-generator.tech
"""

import unittest

import httpretty

import test.mock
import test.utils
from ubiquity import Configuration
from ubiquity.api import (
    fee_estimator_api,
    ApiClient,
)


class TestFeeEstimatorApi(unittest.TestCase):
    """TransactionsApi unit test stubs"""
    def setUp(self):
        self.api_client = ApiClient(Configuration())
        self.api_instance = fee_estimator_api.FeeEstimatorApi(
            self.api_client)  # noqa: E501
        self.protocols = test.utils.get_protocols()

    def tearDown(self):
        pass

    # Helper methods


    @httpretty.activate(verbose=True, allow_net_connect=False)
    def test_estimate_fee(self):
        protocol = "ethereum"
        network = "testnet"

        endpoint = {
            "req_url":
                f"/{protocol}/{network}/tx/estimate_fee",
            "method":
                httpretty.GET,
            "status":
                200,
            "response_data":
                test.mock.get_mock_file_content(
                    "fee_estimator_api/estimate_fee_eth.json")
        }
        test.mock.setup_mock_server(self.api_client.configuration.host,
                                    [endpoint])
        fee = self.api_instance.get_fee_estimate(protocol, network)

        expected_response = {
            "most_recent_block": 14520089,
            "estimated_fees": {
                "fast": {
                    "max_priority_fee": 1500000000,
                    "max_total_fee": 104286077619
                },
                "medium": {
                    "max_priority_fee": 1200000000,
                    "max_total_fee": 75525785588
                },
                "slow": {
                    "max_priority_fee": 1000000000,
                    "max_total_fee": 61478948333
                }
            }
        }

        assert fee["most_recent_block"] == expected_response["most_recent_block"]
        assert fee["estimated_fees"]["fast"]["max_priority_fee"] == expected_response["estimated_fees"]["fast"]["max_priority_fee"]  # to note using any type in python the numerical value defaults to float
        assert fee["estimated_fees"]["fast"]["max_total_fee"] == expected_response["estimated_fees"]["fast"]["max_total_fee"]  # to note using any type in python the numerical value defaults to float
        assert fee["estimated_fees"]["medium"]["max_priority_fee"] == expected_response["estimated_fees"]["medium"]["max_priority_fee"]  # to note using any type in python the numerical value defaults to float
        assert fee["estimated_fees"]["medium"]["max_total_fee"] == expected_response["estimated_fees"]["medium"]["max_total_fee"]  # to note using any type in python the numerical value defaults to float
        assert fee["estimated_fees"]["slow"]["max_priority_fee"] == expected_response["estimated_fees"]["slow"]["max_priority_fee"]  # to note using any type in python the numerical value defaults to float
        assert fee["estimated_fees"]["slow"]["max_total_fee"] == expected_response["estimated_fees"]["slow"]["max_total_fee"]  # to note using any type in python the numerical value defaults to float



if __name__ == '__main__':
    unittest.main()
