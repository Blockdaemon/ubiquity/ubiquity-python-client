# BlockIdentifier


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **int** | The block number. | [optional] 
**id** | **str** | The block hash. | [optional] 
**parent_id** | **str** | The parent block hash. | [optional] 
**date** | **int** | The block date in Unix timestamp format. | [optional] 
**num_txs** | **int** | The amount of transaction in the block. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


