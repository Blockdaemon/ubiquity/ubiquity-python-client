# ubiquity.ubiquity_openapi_client.BalancesUTXOApi

All URIs are relative to *https://svc.blockdaemon.com/universal/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_list_of_balances_by_address**](BalancesUTXOApi.md#get_list_of_balances_by_address) | **GET** /{protocol}/{network}/account/{address} | Get a List of Balances for an Address
[**get_list_of_balances_by_addresses**](BalancesUTXOApi.md#get_list_of_balances_by_addresses) | **POST** /{protocol}/{network}/accounts/ | Get a List of Balances for Multiple Adresses
[**get_report_by_address**](BalancesUTXOApi.md#get_report_by_address) | **GET** /{protocol}/{network}/account/{address}/report | Get a Financial Report for an Address Between a Time Period


# **get_list_of_balances_by_address**
> Balances get_list_of_balances_by_address()

Get a List of Balances for an Address

Returns a list of account balances by a user-defined account address for the supported currencies.

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import balances__utxo_api
from ubiquity.ubiquity_openapi_client.model.error import Error
from ubiquity.ubiquity_openapi_client.model.balances import Balances
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = balances__utxo_api.BalancesUTXOApi(api_client)
    assets = "ethereum/native/eth" # str | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. Find all the asset paths on this [page](https://docs.blockdaemon.com/reference/available-currencies-and-tokens). (optional)

    # example passing only required values which don't have defaults set
    try:
        # Get a List of Balances for an Address
        api_response = api_instance.get_list_of_balances_by_address()
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling BalancesUTXOApi->get_list_of_balances_by_address: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Get a List of Balances for an Address
        api_response = api_instance.get_list_of_balances_by_address(assets=assets)
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling BalancesUTXOApi->get_list_of_balances_by_address: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **str**| Protocol handle, one of:  &#x60;algorand&#x60;, &#x60;avalanche&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;fantom&#x60;, &#x60;litecoin&#x60;, &#x60;near&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60;, &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | defaults to "bitcoin"
 **network** | **str**| Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | defaults to "mainnet"
 **address** | **str**| The account address of the protocol. | defaults to "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa"
 **assets** | **str**| Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. Find all the asset paths on this [page](https://docs.blockdaemon.com/reference/available-currencies-and-tokens). | [optional]

### Return type

[**Balances**](Balances.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Balances |  -  |
**400** | Bad request |  -  |
**401** | Invalid or expired token |  -  |
**404** | Not Found |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_list_of_balances_by_addresses**
> AccountsBalances get_list_of_balances_by_addresses(accounts_obj)

Get a List of Balances for Multiple Adresses

Returns a list of account balances by a user-defined list of account addresses for the supported currencies. The maximum of account addresses allowed in the filter is 10. 

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import balances__utxo_api
from ubiquity.ubiquity_openapi_client.model.accounts_balances import AccountsBalances
from ubiquity.ubiquity_openapi_client.model.accounts_obj import AccountsObj
from ubiquity.ubiquity_openapi_client.model.error import Error
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = balances__utxo_api.BalancesUTXOApi(api_client)
    accounts_obj = AccountsObj(
        addresses=["1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa","bc1qex0aqq8mxqfh4cpl62eg755836djjx20yzuuu8"],
    ) # AccountsObj | 
    assets = "ethereum/native/eth" # str | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. Find all the asset paths on this [page](https://docs.blockdaemon.com/reference/available-currencies-and-tokens). (optional)

    # example passing only required values which don't have defaults set
    try:
        # Get a List of Balances for Multiple Adresses
        api_response = api_instance.get_list_of_balances_by_addresses(accounts_obj)
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling BalancesUTXOApi->get_list_of_balances_by_addresses: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Get a List of Balances for Multiple Adresses
        api_response = api_instance.get_list_of_balances_by_addresses(accounts_obj, assets=assets)
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling BalancesUTXOApi->get_list_of_balances_by_addresses: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accounts_obj** | [**AccountsObj**](AccountsObj.md)|  |
 **protocol** | **str**| Protocol handle, one of:  &#x60;algorand&#x60;, &#x60;avalanche&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;fantom&#x60;, &#x60;litecoin&#x60;, &#x60;near&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60;, &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | defaults to "bitcoin"
 **network** | **str**| Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | defaults to "mainnet"
 **assets** | **str**| Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. Find all the asset paths on this [page](https://docs.blockdaemon.com/reference/available-currencies-and-tokens). | [optional]

### Return type

[**AccountsBalances**](AccountsBalances.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Balances |  -  |
**400** | Bad request |  -  |
**401** | Invalid or expired token |  -  |
**404** | Not Found |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_report_by_address**
> Report get_report_by_address()

Get a Financial Report for an Address Between a Time Period

Returns a financial report by a user-defined account address between a time period. Default timescale is within the last 30 days. 

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import balances__utxo_api
from ubiquity.ubiquity_openapi_client.model.error import Error
from ubiquity.ubiquity_openapi_client.model.report import Report
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = balances__utxo_api.BalancesUTXOApi(api_client)
    _from = 961846434 # int | The Unix Timestamp from where to start. (optional)
    to = 1119612834 # int | The Unix Timestamp from where to end. (optional)
    page_token = "xyz" # str | The token to retrieve more items in the next request. Use the `next_page_token` returned from the previous response for this parameter. (optional)
    page_size = 1000 # int | The max number of items to return in a response. Defaults to 50k and is capped at 100k.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Get a Financial Report for an Address Between a Time Period
        api_response = api_instance.get_report_by_address()
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling BalancesUTXOApi->get_report_by_address: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Get a Financial Report for an Address Between a Time Period
        api_response = api_instance.get_report_by_address(_from=_from, to=to, page_token=page_token, page_size=page_size)
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling BalancesUTXOApi->get_report_by_address: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **str**| Protocol handle, one of: &#x60;algorand&#x60;, &#x60;fantom&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;stellar&#x60; &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | defaults to "polkadot"
 **network** | **str**| Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | defaults to "mainnet"
 **address** | **str**| The account address of the protocol. | defaults to "12yi4uHFbnSUryffXT7Xq92fbGC3iXvCs3vz9HjVgpb4sBvL"
 **_from** | **int**| The Unix Timestamp from where to start. | [optional]
 **to** | **int**| The Unix Timestamp from where to end. | [optional]
 **page_token** | **str**| The token to retrieve more items in the next request. Use the &#x60;next_page_token&#x60; returned from the previous response for this parameter. | [optional]
 **page_size** | **int**| The max number of items to return in a response. Defaults to 50k and is capped at 100k.  | [optional]

### Return type

[**Report**](Report.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Account Activity |  -  |
**400** | Bad request |  -  |
**401** | Invalid or expired token |  -  |
**404** | Not Found |  -  |
**413** | Too Many Transactions |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

