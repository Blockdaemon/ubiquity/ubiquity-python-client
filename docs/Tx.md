# Tx


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Unique transaction identifier. | [optional] 
**block_id** | **str, none_type** | Block hash if mined, otherwise omitted. | [optional] 
**date** | **int** | The transaction creation unix timestamp. | [optional] 
**status** | **str** | Result status of the transaction. | [optional] 
**num_events** | **int** | List of transaction events. | [optional] 
**meta** | **{str: (bool, date, datetime, dict, float, int, list, str, none_type)}** | Protocol specific data that doesn&#39;t fit into a standard model. | [optional] 
**block_number** | **int, none_type** | Block number if mined, otherwise omitted. | [optional] 
**confirmations** | **int** | Total transaction confirmations. | [optional] 
**events** | [**[Event], none_type**](Event.md) |  | [optional] 
**assets** | **[str], none_type** | List of moved assets by asset path. Find all the asset paths on this [page](https://docs.blockdaemon.com/reference/available-currencies-and-tokens). | [optional] 
**nonce** | **int** | The nonce of the transaction. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


