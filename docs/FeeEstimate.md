# FeeEstimate


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fast** | **int** | The fast maximum priority fee. | [optional] 
**medium** | **int** | The medium maximum priority fee. | [optional] 
**slow** | **int** | The slow maximum priority fee. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


