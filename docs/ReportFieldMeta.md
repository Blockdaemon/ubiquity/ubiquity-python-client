# ReportFieldMeta

Additional metadata bespoke to specific protocols.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | The type of protocol the metadata relates to. | [optional]  if omitted the server will use the default value of "algorand_meta"
**sender_reward** | **str** | The sender reward amount. | [optional] 
**recipient_reward** | **str** | The recipient reward amount. | [optional] 
**close** | **str** | The closing account. | [optional] 
**close_amount** | **str** |  The closing amount. | [optional] 
**close_reward** | **str** | The closing reward. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


