# Balance

Account balance object.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | [**Currency**](Currency.md) |  | [optional] 
**confirmed_balance** | **str** | The balance that can be spent. | [optional] 
**pending_balance** | **str** | The balance value from the latest confirmed block. | [optional] 
**confirmed_block** | **int** | The confirmed block number. | [optional] 
**confirmed_nonce** | **int** | The confirmed nonce of the transaction. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


