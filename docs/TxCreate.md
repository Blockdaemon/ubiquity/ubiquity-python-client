# TxCreate


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_from** | **str** | The source UTXO or account ID for the originating funds. | 
**to** | [**[TxDestination]**](TxDestination.md) | A list of recipients. | 
**contract** | [**TxCreateContract**](TxCreateContract.md) |  | [optional] 
**index** | **int** | The UTXO index or the account Nonce. Required for Bitcoin, Bitcoincash, and Litecoin. | [optional] 
**fee** | **str** | The fee you are willing to pay for the transaction. For Ethereum and Polygon see &#39;protocol.ethereum&#39; and &#39;protocol.polygon&#39;. | [optional] 
**protocol** | **TxCreateProtocol** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


