# ubiquity.ubiquity_openapi_client.ProtocolAndEndpointSupportApi

All URIs are relative to *https://svc.blockdaemon.com/universal/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_protocol_endpoints**](ProtocolAndEndpointSupportApi.md#get_protocol_endpoints) | **GET** /{protocol}/{network} | Get the Protocol Info
[**get_protocols_list**](ProtocolAndEndpointSupportApi.md#get_protocols_list) | **GET** / | Get the Protocols Overview


# **get_protocol_endpoints**
> ProtocolDetail get_protocol_endpoints()

Get the Protocol Info

Provides information about supported endpoints and generic protocol information. 

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import protocol_and_endpoint_support_api
from ubiquity.ubiquity_openapi_client.model.protocol_detail import ProtocolDetail
from ubiquity.ubiquity_openapi_client.model.error import Error
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = protocol_and_endpoint_support_api.ProtocolAndEndpointSupportApi(api_client)

    # example passing only required values which don't have defaults set
    try:
        # Get the Protocol Info
        api_response = api_instance.get_protocol_endpoints()
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling ProtocolAndEndpointSupportApi->get_protocol_endpoints: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **str**| Protocol handle, one of:  &#x60;algorand&#x60;, &#x60;avalanche&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;fantom&#x60;, &#x60;litecoin&#x60;, &#x60;near&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60;, &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | defaults to "bitcoin"
 **network** | **str**| Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | defaults to "mainnet"

### Return type

[**ProtocolDetail**](ProtocolDetail.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Protocol overview |  -  |
**401** | Invalid or expired token |  -  |
**404** | Not Found |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_protocols_list**
> ProtocolsOverview get_protocols_list()

Get the Protocols Overview

Provides a list of supported protocols and networks. 

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import protocol_and_endpoint_support_api
from ubiquity.ubiquity_openapi_client.model.protocols_overview import ProtocolsOverview
from ubiquity.ubiquity_openapi_client.model.error import Error
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = protocol_and_endpoint_support_api.ProtocolAndEndpointSupportApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Get the Protocols Overview
        api_response = api_instance.get_protocols_list()
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling ProtocolAndEndpointSupportApi->get_protocols_list: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**ProtocolsOverview**](ProtocolsOverview.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Protocols overview |  -  |
**401** | Invalid or expired token |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

