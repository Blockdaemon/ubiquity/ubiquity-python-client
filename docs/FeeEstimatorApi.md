# ubiquity.ubiquity_openapi_client.FeeEstimatorApi

All URIs are relative to *https://svc.blockdaemon.com/universal/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_fee_estimate**](FeeEstimatorApi.md#get_fee_estimate) | **GET** /{protocol}/{network}/tx/estimate_fee | Get the Fee Estimation


# **get_fee_estimate**
> InlineResponse200 get_fee_estimate()

Get the Fee Estimation

Returns fee estimation in decimals.This endpoint will return 3 fee estimations: fast, medium and slow. 

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import fee_estimator_api
from ubiquity.ubiquity_openapi_client.model.error import Error
from ubiquity.ubiquity_openapi_client.model.inline_response200 import InlineResponse200
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = fee_estimator_api.FeeEstimatorApi(api_client)

    # example passing only required values which don't have defaults set
    try:
        # Get the Fee Estimation
        api_response = api_instance.get_fee_estimate()
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling FeeEstimatorApi->get_fee_estimate: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **str**| Protocol handle, one of: &#x60;avalanche&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;ethereum&#x60;, &#x60;fantom&#x60;, &#x60;litecoin&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60; and &#x60;solana&#x60;.  | defaults to "bitcoin"
 **network** | **str**| Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | defaults to "mainnet"

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Protocols Overview |  -  |
**401** | Invalid or expired token |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

