# ubiquity.ubiquity_openapi_client.BlocksApi

All URIs are relative to *https://svc.blockdaemon.com/universal/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_block_by_number**](BlocksApi.md#get_block_by_number) | **GET** /{protocol}/{network}/block/{block_identifier} | Get a Block by Number or Hash
[**get_block_identifier_by_number**](BlocksApi.md#get_block_identifier_by_number) | **GET** /{protocol}/{network}/block_identifier/{block_identifier} | Get a Block Identifier by Number of Hash
[**get_block_identifiers**](BlocksApi.md#get_block_identifiers) | **GET** /{protocol}/{network}/block_identifiers | Get a List of Block Identifiers
[**get_current_block_hash**](BlocksApi.md#get_current_block_hash) | **GET** /{protocol}/{network}/sync/block_id | Get the Current Block Hash
[**get_current_block_number**](BlocksApi.md#get_current_block_number) | **GET** /{protocol}/{network}/sync/block_number | Get the Current Block Number


# **get_block_by_number**
> Block get_block_by_number()

Get a Block by Number or Hash

Get a block and all its transactions by a user-defined block number or block hash.   Use `-1` or `current` parameter to return the current block. 

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import blocks_api
from ubiquity.ubiquity_openapi_client.model.block import Block
from ubiquity.ubiquity_openapi_client.model.error import Error
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = blocks_api.BlocksApi(api_client)

    # example passing only required values which don't have defaults set
    try:
        # Get a Block by Number or Hash
        api_response = api_instance.get_block_by_number()
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling BlocksApi->get_block_by_number: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **str**| Protocol handle, one of:  &#x60;algorand&#x60;, &#x60;avalanche&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;fantom&#x60;, &#x60;litecoin&#x60;, &#x60;near&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60;, &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | defaults to "bitcoin"
 **network** | **str**| Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | defaults to "mainnet"
 **block_identifier** | **str**| The block identifier, hash or number. | defaults to "0000000000000000000590fc0f3eba193a278534220b2b37e9849e1a770ca959"

### Return type

[**Block**](Block.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Block |  -  |
**400** | Invalid Block Identifier |  -  |
**401** | Invalid or expired token |  -  |
**404** | Not Found |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_block_identifier_by_number**
> BlockIdentifier get_block_identifier_by_number()

Get a Block Identifier by Number of Hash

Get the minimal block data, block header, by a user-defined block number or block hash.   Use `-1` or `current` parameter to return the current block. 

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import blocks_api
from ubiquity.ubiquity_openapi_client.model.error import Error
from ubiquity.ubiquity_openapi_client.model.block_identifier import BlockIdentifier
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = blocks_api.BlocksApi(api_client)

    # example passing only required values which don't have defaults set
    try:
        # Get a Block Identifier by Number of Hash
        api_response = api_instance.get_block_identifier_by_number()
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling BlocksApi->get_block_identifier_by_number: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **str**| Protocol handle, one of:  &#x60;algorand&#x60;, &#x60;avalanche&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;fantom&#x60;, &#x60;litecoin&#x60;, &#x60;near&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60;, &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | defaults to "bitcoin"
 **network** | **str**| Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | defaults to "mainnet"
 **block_identifier** | **str**| The block identifier, hash or number. | defaults to "0000000000000000000590fc0f3eba193a278534220b2b37e9849e1a770ca959"

### Return type

[**BlockIdentifier**](BlockIdentifier.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Block |  -  |
**400** | Invalid Block hash |  -  |
**401** | Invalid or expired token |  -  |
**404** | Not Found |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_block_identifiers**
> BlockIdentifiers get_block_identifiers()

Get a List of Block Identifiers

Returns a list of minimal block data, block headers such as block hash and block number. 

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import blocks_api
from ubiquity.ubiquity_openapi_client.model.block_identifiers import BlockIdentifiers
from ubiquity.ubiquity_openapi_client.model.error import Error
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = blocks_api.BlocksApi(api_client)
    order = "desc" # str | The pagination order. (optional) if omitted the server will use the default value of "desc"
    page_token = "8185.123" # str | The token to retrieve more items in the next request. Use the `next_page_token` returned from the previous response for this parameter. (optional)
    page_size = 25 # int | Max number of items to return in a response. Defaults to 25 and is capped at 100.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Get a List of Block Identifiers
        api_response = api_instance.get_block_identifiers()
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling BlocksApi->get_block_identifiers: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Get a List of Block Identifiers
        api_response = api_instance.get_block_identifiers(order=order, page_token=page_token, page_size=page_size)
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling BlocksApi->get_block_identifiers: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **str**| Protocol handle, one of: &#x60;avalanche&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;fantom&#x60;, &#x60;litecoin&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;tezos&#x60;.  | defaults to "bitcoin"
 **network** | **str**| Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | defaults to "mainnet"
 **order** | **str**| The pagination order. | [optional] if omitted the server will use the default value of "desc"
 **page_token** | **str**| The token to retrieve more items in the next request. Use the &#x60;next_page_token&#x60; returned from the previous response for this parameter. | [optional]
 **page_size** | **int**| Max number of items to return in a response. Defaults to 25 and is capped at 100.  | [optional]

### Return type

[**BlockIdentifiers**](BlockIdentifiers.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Block headers |  -  |
**400** | Bad request |  -  |
**401** | Invalid or expired token |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_current_block_hash**
> str get_current_block_hash()

Get the Current Block Hash

Get the current block id (hash) of the protocol.

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import blocks_api
from ubiquity.ubiquity_openapi_client.model.error import Error
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = blocks_api.BlocksApi(api_client)

    # example passing only required values which don't have defaults set
    try:
        # Get the Current Block Hash
        api_response = api_instance.get_current_block_hash()
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling BlocksApi->get_current_block_hash: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **str**| Protocol handle, one of:  &#x60;algorand&#x60;, &#x60;avalanche&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;fantom&#x60;, &#x60;litecoin&#x60;, &#x60;near&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60;, &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | defaults to "bitcoin"
 **network** | **str**| Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | defaults to "mainnet"

### Return type

**str**

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Current block hash |  -  |
**401** | Invalid or expired token |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_current_block_number**
> int get_current_block_number()

Get the Current Block Number

Returns the current block number/height of the protocol.

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import blocks_api
from ubiquity.ubiquity_openapi_client.model.error import Error
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = blocks_api.BlocksApi(api_client)

    # example passing only required values which don't have defaults set
    try:
        # Get the Current Block Number
        api_response = api_instance.get_current_block_number()
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling BlocksApi->get_current_block_number: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **str**| Protocol handle, one of:  &#x60;algorand&#x60;, &#x60;avalanche&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;fantom&#x60;, &#x60;litecoin&#x60;, &#x60;near&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60;, &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | defaults to "bitcoin"
 **network** | **str**| Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | defaults to "mainnet"

### Return type

**int**

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Current block number |  -  |
**400** | Bad request |  -  |
**401** | Invalid or expired token |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

