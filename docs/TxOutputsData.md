# TxOutputsData


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **str** | The status of the given transaction output. | [optional] 
**is_spent** | **bool** | Whether the transaction output was spent or not. | [optional] 
**value** | **int** | The amount of tokens within the given output. | [optional] 
**spent** | **dict** |  | [optional] 
**mined** | **dict** |  | [optional] 
**pending** | **dict** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


