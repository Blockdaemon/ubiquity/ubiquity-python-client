# TxCreateEvm

EVM specific parameters for transaction creation.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gas** | **int** | Gas of transaction. If omitted, the gas will be estimated using eth_estimateGas. | [optional] 
**max_priority_fee_per_gas** | **int** | The max priority fee per gas. Also referred to as the miner tip. When omitted an estimate will be used. | [optional] 
**max_fee_per_gas** | **int** | The max fee per gas, this includes the base fee and max priority fee per gas. When omitted an esitmate will be used. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


