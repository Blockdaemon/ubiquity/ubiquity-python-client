# TxOutputResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **int** | The output index within a given transaction. | [optional] 
**tx_id** | **str** | The transaction identifier. | [optional] 
**date** | **int** | The transaction creation unix timestamp. | [optional] 
**block_id** | **str** | The hash identifier of the block which the transaction was mined. | [optional] 
**block_number** | **int** | The number of the block which the transaction was mined. | [optional] 
**confirmations** | **int** | The number of confirmations the transaction took in order to be mined. | [optional] 
**meta** | [**TxOutputResponseMeta**](TxOutputResponseMeta.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


