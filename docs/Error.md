# Error


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | The HTTP error type. | [optional] 
**code** | **int** | The numeric error code. | [optional] 
**title** | **str** | The short error description. | [optional] 
**status** | **int** | The HTTP status of the error. | [optional] 
**detail** | **str** | Long error description | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


