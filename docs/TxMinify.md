# TxMinify

Transaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **int** | The output index within a given transaction. | [optional] 
**tx_id** | **str** | The transaction identifier. | [optional] 
**date** | **int** | The Unix timestamp. | [optional] 
**block_id** | **str, none_type** | The ID of block. | [optional] 
**block_number** | **int, none_type** | The height of block. | [optional] 
**confirmations** | **int** | Total transaction confirmations. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


