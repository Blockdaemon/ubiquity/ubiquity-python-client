# ProtocolDetail


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**handle** | **str** | The protocol handle. | [optional] 
**network** | **bool, date, datetime, dict, float, int, list, str, none_type** | The specified network type. | [optional] 
**genesis_number** | **int** | The genesis number. | [optional] 
**endpoints** | **[str]** | The supported endpoints. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


