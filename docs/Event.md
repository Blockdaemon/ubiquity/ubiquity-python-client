# Event


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The event identifier. | [optional] 
**transaction_id** | **str** | The transaction identifer this event is presented. | [optional] 
**type** | **str** | The event type. | [optional] 
**denomination** | **str, none_type** | The Symbol of currency, can be natibe currency or token currency. | [optional] 
**destination** | **str, none_type** | The destination address of the event. | [optional] 
**source** | **str, none_type** | The source address of the event. | [optional] 
**meta** | [**EventMeta**](EventMeta.md) |  | [optional] 
**date** | **int** | The event date in unix timestamp format. | [optional] 
**amount** | **int, none_type** | The coin amount transfered in the event. | [optional] 
**decimals** | **int, none_type** | The coin amount transfered in the event. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


