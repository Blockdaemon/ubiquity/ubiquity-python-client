# Report


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fields** | [**[ReportField]**](ReportField.md) | The transaction items. | 
**items** | **int** | The number of transactions in the report. | 
**limit** | **int** | The limit number. | [optional] 
**page_size** | **int** | The limit number provided in the request or the default. | [optional] 
**page_token** | **str** | The token to retrieve more items in the next request. Use the &#x60;next_page_token&#x60; returned from the previous response for this parameter. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


