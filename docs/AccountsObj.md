# AccountsObj


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**addresses** | **[str]** | A list of addresses. | [optional]  if omitted the server will use the default value of ["1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa","bc1qex0aqq8mxqfh4cpl62eg755836djjx20yzuuu8"]
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


