# SmartTokenCurrency


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asset_path** | **str** | The asset path of the transferred currency. | 
**type** | **str** | The type of the currency. | defaults to "smart_token"
**symbol** | **str** | The currency symbol. | [optional] 
**name** | **str** | The name of the currency. | [optional] 
**decimals** | **int** | The decimal places right to the comma. | [optional] 
**detail** | [**SmartToken**](SmartToken.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


