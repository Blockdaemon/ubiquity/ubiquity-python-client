# TxOutputResponseMeta

The meta details of the transaction.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**addresses** | **[str]** | A list of addresses. | [optional] 
**index** | **int** | The index number. | [optional] 
**script** | **str** | The script. | [optional] 
**script_type** | **str** | The script type. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


