# ubiquity.ubiquity_openapi_client.TransactionsApi

All URIs are relative to *https://svc.blockdaemon.com/universal/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_tx_by_hash**](TransactionsApi.md#get_tx_by_hash) | **GET** /{protocol}/{network}/tx/{hash} | Get a Transaction
[**get_tx_confirmations**](TransactionsApi.md#get_tx_confirmations) | **GET** /{protocol}/{network}/tx/{hash}/confirmations | Get the Transaction Confirmations
[**get_tx_output_by_hash_and_index**](TransactionsApi.md#get_tx_output_by_hash_and_index) | **GET** /{protocol}/{network}/tx/{hash}/{index} | Get a Transaction Output by Hash and Index
[**get_txs**](TransactionsApi.md#get_txs) | **GET** /{protocol}/{network}/txs | Get a List of Transactions
[**get_txs_by_address**](TransactionsApi.md#get_txs_by_address) | **GET** /{protocol}/{network}/account/{address}/txs | Get a List of Transactions for a Given Address
[**get_utxoby_account**](TransactionsApi.md#get_utxoby_account) | **GET** /{protocol}/{network}/account/{address}/utxo | Get a List of Transaction Inputs and Outputs
[**tx_create**](TransactionsApi.md#tx_create) | **POST** /{protocol}/{network}/tx/create | Create an Unsigned Transaction
[**tx_send**](TransactionsApi.md#tx_send) | **POST** /{protocol}/{network}/tx/send | Submit a Signed Transaction


# **get_tx_by_hash**
> Tx get_tx_by_hash()

Get a Transaction

Returns a transaction by a user-defined transaction hash.

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import transactions_api
from ubiquity.ubiquity_openapi_client.model.tx import Tx
from ubiquity.ubiquity_openapi_client.model.error import Error
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = transactions_api.TransactionsApi(api_client)

    # example passing only required values which don't have defaults set
    try:
        # Get a Transaction
        api_response = api_instance.get_tx_by_hash()
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling TransactionsApi->get_tx_by_hash: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **str**| Protocol handle, one of: &#x60;algorand&#x60;, &#x60;avalanche&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;fantom&#x60;, &#x60;litecoin&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60; &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | defaults to "bitcoin"
 **network** | **str**| Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | defaults to "mainnet"
 **hash** | **str**| The transaction hash. | defaults to "71d4f3412ec11128bbd9ce988d5bff2ec3bb6ea3953c8faf189d88ae49de9f7a"

### Return type

[**Tx**](Tx.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Transaction |  -  |
**401** | Invalid or expired token |  -  |
**404** | Not Found |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_tx_confirmations**
> TxConfirmation get_tx_confirmations()

Get the Transaction Confirmations

Returns the number of transaction confirmations by a user-defined transaction hash. 

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import transactions_api
from ubiquity.ubiquity_openapi_client.model.error import Error
from ubiquity.ubiquity_openapi_client.model.tx_confirmation import TxConfirmation
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = transactions_api.TransactionsApi(api_client)

    # example passing only required values which don't have defaults set
    try:
        # Get the Transaction Confirmations
        api_response = api_instance.get_tx_confirmations()
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling TransactionsApi->get_tx_confirmations: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **str**| Protocol handle, one of: &#x60;avalanche&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;fantom&#x60;, &#x60;litecoin&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;tezos&#x60;.  | defaults to "bitcoin"
 **network** | **str**| Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | defaults to "mainnet"
 **hash** | **str**| The transaction hash. | defaults to "71d4f3412ec11128bbd9ce988d5bff2ec3bb6ea3953c8faf189d88ae49de9f7a"

### Return type

[**TxConfirmation**](TxConfirmation.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Transaction confirmations |  -  |
**401** | Invalid or expired token |  -  |
**404** | Not Found |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_tx_output_by_hash_and_index**
> TxOutput get_tx_output_by_hash_and_index()

Get a Transaction Output by Hash and Index

Get a transaction output by a user-defined transaction hash and the transaction output index.

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import transactions_api
from ubiquity.ubiquity_openapi_client.model.tx_output import TxOutput
from ubiquity.ubiquity_openapi_client.model.error import Error
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = transactions_api.TransactionsApi(api_client)

    # example passing only required values which don't have defaults set
    try:
        # Get a Transaction Output by Hash and Index
        api_response = api_instance.get_tx_output_by_hash_and_index()
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling TransactionsApi->get_tx_output_by_hash_and_index: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **str**| Protocol handle, one of: &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;litecoin&#x60;.  | defaults to "bitcoin"
 **network** | **str**| Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | defaults to "mainnet"
 **hash** | **str**| The transaction hash. | defaults to "71d4f3412ec11128bbd9ce988d5bff2ec3bb6ea3953c8faf189d88ae49de9f7a"
 **index** | **int**| The transaction output index. | defaults to 0

### Return type

[**TxOutput**](TxOutput.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Transaction output. |  -  |
**401** | Invalid or expired token |  -  |
**404** | Not Found |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_txs**
> TxPage get_txs()

Get a List of Transactions

Get a List of transactions, starting with the lastest one. Each call returns an array of the entire list. 

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import transactions_api
from ubiquity.ubiquity_openapi_client.model.error import Error
from ubiquity.ubiquity_openapi_client.model.tx_page import TxPage
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = transactions_api.TransactionsApi(api_client)
    block_hash = "0x2444165297806ad5598e4569e5823b3df0cde3e48b346781ab632fa6cef1a0ec" # str | Filter by block hash. You can specify only one block hash at a time. (optional)
    block_number = 789387 # int | Filter by block number. (optional)
    assets = "ethereum/native/eth" # str | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. Find all the asset paths on this [page](https://docs.blockdaemon.com/reference/available-currencies-and-tokens). (optional)
    order = "desc" # str | The pagination order. (optional) if omitted the server will use the default value of "desc"
    page_token = "8185.123" # str | The token to retrieve more items in the next request. Use the `next_page_token` returned from the previous response for this parameter. (optional)
    page_size = 25 # int | Max number of items to return in a response. Defaults to 25 and is capped at 100.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Get a List of Transactions
        api_response = api_instance.get_txs()
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling TransactionsApi->get_txs: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Get a List of Transactions
        api_response = api_instance.get_txs(block_hash=block_hash, block_number=block_number, assets=assets, order=order, page_token=page_token, page_size=page_size)
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling TransactionsApi->get_txs: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **str**| Protocol handle, one of: &#x60;algorand&#x60;, &#x60;avalanche&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;fantom&#x60; &#x60;litecoin&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;stellar&#x60; &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | defaults to "bitcoin"
 **network** | **str**| Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | defaults to "mainnet"
 **block_hash** | **str**| Filter by block hash. You can specify only one block hash at a time. | [optional]
 **block_number** | **int**| Filter by block number. | [optional]
 **assets** | **str**| Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. Find all the asset paths on this [page](https://docs.blockdaemon.com/reference/available-currencies-and-tokens). | [optional]
 **order** | **str**| The pagination order. | [optional] if omitted the server will use the default value of "desc"
 **page_token** | **str**| The token to retrieve more items in the next request. Use the &#x60;next_page_token&#x60; returned from the previous response for this parameter. | [optional]
 **page_size** | **int**| Max number of items to return in a response. Defaults to 25 and is capped at 100.  | [optional]

### Return type

[**TxPage**](TxPage.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Transactions |  -  |
**401** | Invalid or expired token |  -  |
**403** | Invalid continuation |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_txs_by_address**
> TxPage get_txs_by_address()

Get a List of Transactions for a Given Address

Returns the transactions that an address was involved with, from newest to oldest. 

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import transactions_api
from ubiquity.ubiquity_openapi_client.model.error import Error
from ubiquity.ubiquity_openapi_client.model.tx_page import TxPage
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = transactions_api.TransactionsApi(api_client)
    assets = "ethereum/native/eth" # str | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. Find all the asset paths on this [page](https://docs.blockdaemon.com/reference/available-currencies-and-tokens). (optional)
    _from = 961846434 # int | Unix Timestamp from where to start (optional)
    to = 1119612834 # int | Unix Timestamp from where to end (optional)
    order = "desc" # str | The pagination order. (optional) if omitted the server will use the default value of "desc"
    page_token = "8185.123" # str | The token to retrieve more items in the next request. Use the `next_page_token` returned from the previous response for this parameter. (optional)
    page_size = 25 # int | Max number of items to return in a response. Defaults to 25 and is capped at 100.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Get a List of Transactions for a Given Address
        api_response = api_instance.get_txs_by_address()
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling TransactionsApi->get_txs_by_address: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Get a List of Transactions for a Given Address
        api_response = api_instance.get_txs_by_address(assets=assets, _from=_from, to=to, order=order, page_token=page_token, page_size=page_size)
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling TransactionsApi->get_txs_by_address: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **str**| The protocol handle, one of: &#x60;algorand&#x60;, &#x60;avalanche&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;fantom&#x60;, &#x60;litecoin&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;tezos&#x60;, &#x60;stellar&#x60;, &#x60;xrp&#x60;.  | defaults to "bitcoin"
 **network** | **str**| Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | defaults to "mainnet"
 **address** | **str**| The account address of the protocol. | defaults to "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa"
 **assets** | **str**| Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. Find all the asset paths on this [page](https://docs.blockdaemon.com/reference/available-currencies-and-tokens). | [optional]
 **_from** | **int**| Unix Timestamp from where to start | [optional]
 **to** | **int**| Unix Timestamp from where to end | [optional]
 **order** | **str**| The pagination order. | [optional] if omitted the server will use the default value of "desc"
 **page_token** | **str**| The token to retrieve more items in the next request. Use the &#x60;next_page_token&#x60; returned from the previous response for this parameter. | [optional]
 **page_size** | **int**| Max number of items to return in a response. Defaults to 25 and is capped at 100.  | [optional]

### Return type

[**TxPage**](TxPage.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Transactions |  -  |
**400** | Invalid address |  -  |
**401** | Invalid or expired token |  -  |
**403** | Invalid continuation |  -  |
**404** | Not Found |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_utxoby_account**
> TxOutputs get_utxoby_account()

Get a List of Transaction Inputs and Outputs

Returns the transaction inputs and outputs following the BTC's UTXO model definition by a user-definied account address. 

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import transactions_api
from ubiquity.ubiquity_openapi_client.model.error import Error
from ubiquity.ubiquity_openapi_client.model.tx_outputs import TxOutputs
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = transactions_api.TransactionsApi(api_client)
    spent = True # bool | Whether the transaction output was spent or not. (optional)
    check_mempool = True # bool | Whether to check for UTXOs spent in the mempool as well as UTXOs pending in the mempool. (optional)
    _from = 961846434 # int | The Unix Timestamp from where to start. (optional)
    to = 1119612834 # int | The Unix Timestamp from where to end. (optional)
    order = "desc" # str | The pagination order. (optional) if omitted the server will use the default value of "desc"
    page_token = "8185.123" # str | The token to retrieve more items in the next request. Use the `next_page_token` returned from the previous response for this parameter. (optional)
    page_size = 1000 # int | The max number of items to return in a response Defaults to 50k and is capped at 100k.  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Get a List of Transaction Inputs and Outputs
        api_response = api_instance.get_utxoby_account()
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling TransactionsApi->get_utxoby_account: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Get a List of Transaction Inputs and Outputs
        api_response = api_instance.get_utxoby_account(spent=spent, check_mempool=check_mempool, _from=_from, to=to, order=order, page_token=page_token, page_size=page_size)
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling TransactionsApi->get_utxoby_account: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **protocol** | **str**| The protocol handle, one of: &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;litecoin&#x60;.  | defaults to "bitcoin"
 **network** | **str**| Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | defaults to "mainnet"
 **address** | **str**| The account address of the protocol. | defaults to "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa"
 **spent** | **bool**| Whether the transaction output was spent or not. | [optional]
 **check_mempool** | **bool**| Whether to check for UTXOs spent in the mempool as well as UTXOs pending in the mempool. | [optional]
 **_from** | **int**| The Unix Timestamp from where to start. | [optional]
 **to** | **int**| The Unix Timestamp from where to end. | [optional]
 **order** | **str**| The pagination order. | [optional] if omitted the server will use the default value of "desc"
 **page_token** | **str**| The token to retrieve more items in the next request. Use the &#x60;next_page_token&#x60; returned from the previous response for this parameter. | [optional]
 **page_size** | **int**| The max number of items to return in a response Defaults to 50k and is capped at 100k.  | [optional]

### Return type

[**TxOutputs**](TxOutputs.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Transaction outputs filtered by the given parameters |  -  |
**400** | Bad request |  -  |
**401** | Invalid or expired token |  -  |
**404** | Not Found |  -  |
**413** | Too Many Transactions |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tx_create**
> UnsignedTx tx_create(tx_create)

Create an Unsigned Transaction

Creates an unsigned transaction.  **Note** that Ethereum currently only supports singular transaction destinations. 

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import transactions_api
from ubiquity.ubiquity_openapi_client.model.unsigned_tx import UnsignedTx
from ubiquity.ubiquity_openapi_client.model.tx_create import TxCreate
from ubiquity.ubiquity_openapi_client.model.error import Error
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = transactions_api.TransactionsApi(api_client)
    tx_create = TxCreate(
        _from="31c129468975e4ef41135a405000e6428a399a03d023b6627eed4cb95faf19ca",
        to=[
            TxDestination(
                destination="mkHS9ne12qx9pS9VojpwU5xtRd4T7X7ZUt",
                amount="0.00010",
            ),
        ],
        contract=TxCreateContract(
            address="0xaFF4481D10270F50f203E0763e2597776068CBc5",
            type="erc-20",
        ),
        index=5,
        fee="21000",
        protocol=TxCreateProtocol(
            avalanche=TxCreateEvm(
                gas=1,
                max_priority_fee_per_gas=1,
                max_fee_per_gas=1,
            ),
            ethereum=TxCreateEvm(
                gas=1,
                max_priority_fee_per_gas=1,
                max_fee_per_gas=1,
            ),
            fantom=TxCreateEvm(
                gas=1,
                max_priority_fee_per_gas=1,
                max_fee_per_gas=1,
            ),
            polygon=TxCreateEvm(
                gas=1,
                max_priority_fee_per_gas=1,
                max_fee_per_gas=1,
            ),
        ),
    ) # TxCreate | 

    # example passing only required values which don't have defaults set
    try:
        # Create an Unsigned Transaction
        api_response = api_instance.tx_create(tx_create)
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling TransactionsApi->tx_create: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tx_create** | [**TxCreate**](TxCreate.md)|  |
 **protocol** | **str**| Protocol handle, one of: &#x60;avalanche&#x60;, &#x60;bitcoin&#x60;, &#x60;ethereum&#x60;, &#x60;dogecoin&#x60;, &#x60;fantom&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;.  | defaults to "bitcoin"
 **network** | **str**| Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | defaults to "mainnet"

### Return type

[**UnsignedTx**](UnsignedTx.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | An unsigned transaction |  -  |
**400** | Bad Request |  -  |
**401** | Invalid or expired token |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **tx_send**
> TxReceipt tx_send(signed_tx)

Submit a Signed Transaction

Submit a signed transaction to the network.  **Note**: A successful transaction may still be rejected on chain or not processed due to a too low fee. You can monitor successful transactions through Universal websockets. 

### Example

* Api Key Authentication (apiKeyAuthHeader):
* Bearer (Opaque) Authentication (bearerAuth):
```python
import time
import ubiquity.ubiquity_openapi_client
from ubiquity.ubiquity_openapi_client.api import transactions_api
from ubiquity.ubiquity_openapi_client.model.signed_tx import SignedTx
from ubiquity.ubiquity_openapi_client.model.tx_receipt import TxReceipt
from ubiquity.ubiquity_openapi_client.model.error import Error
from pprint import pprint
# Defining the host is optional and defaults to https://svc.blockdaemon.com/universal/v1
# See configuration.py for a list of all supported configuration parameters.
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    host = "https://svc.blockdaemon.com/universal/v1"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure API key authorization: apiKeyAuthHeader
configuration.api_key['apiKeyAuthHeader'] = 'YOUR_API_KEY'

# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['apiKeyAuthHeader'] = 'Bearer'

# Configure Bearer authorization (Opaque): bearerAuth
configuration = ubiquity.ubiquity_openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with ubiquity.ubiquity_openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = transactions_api.TransactionsApi(api_client)
    signed_tx = SignedTx(
        tx="0100000001ca19af5fb94ced7e62b623d0039a398a42e60050405a1341efe475894629c131010000008b483045022100d77b002b3142013b3f825a730f5bc3ead2014266f07ba4449269af0cf6f086310220365bca1d616ba86fac42ad69efd5f92c5ed6cf16f27ebf5ab55010efc72c219d014104417eb0abe69db2eca63c84eb44266c29c24973dc81cde16ca86c9d923630cb5f797bae7d7fab13498e06146111356eb271da74add05ebda8f72ff2b2878fddb7ffffffff0410270000000000001976a914344a0f48ca150ec2b903817660b9b68b13a6702688ac204e0000000000001976a914344a0f48ca150ec2b903817660b9b68b13a6702688ac30750000000000001976a914344a0f48ca150ec2b903817660b9b68b13a6702688ac48710000000000001976a914d6fa8814924b480fa7ff903b5ef61100ab4d92fe88ac00000000",
    ) # SignedTx | 

    # example passing only required values which don't have defaults set
    try:
        # Submit a Signed Transaction
        api_response = api_instance.tx_send(signed_tx)
        pprint(api_response)
    except ubiquity.ubiquity_openapi_client.ApiException as e:
        print("Exception when calling TransactionsApi->tx_send: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **signed_tx** | [**SignedTx**](SignedTx.md)|  |
 **protocol** | **str**| Protocol handle, one of: &#x60;algorand&#x60;, &#x60;avalanche&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;fantom&#x60;, &#x60;litecoin&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;.  | defaults to "bitcoin"
 **network** | **str**| Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | defaults to "mainnet"

### Return type

[**TxReceipt**](TxReceipt.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | A submitted Transaction ID |  -  |
**400** | Bad request |  -  |
**401** | Invalid or expired token |  -  |
**429** | Rate limit exceeded |  -  |
**500** | An internal server error happened |  -  |
**503** | The resource you are trying to access is currently unavailable |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

