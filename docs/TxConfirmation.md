# TxConfirmation


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current_height** | **int** | The current block height. | [optional] 
**tx_id** | **str** | The transaction hash. | [optional] 
**confirmations** | **int** | The total transaction confirmations. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


