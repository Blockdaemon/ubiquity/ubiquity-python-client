import ubiquity
from ubiquity.ubiquity_openapi_client import ApiClient as ApiClient_
from ubiquity.ubiquity_openapi_client.api import (
    balances__utxo_api,
    blocks_api,
    protocol_and_endpoint_support_api,
    fee_estimator_api,
    transactions_api,
)


class ApiClient(ApiClient_):
    def __init__(self, configuration):
        super(ApiClient, self).__init__(configuration)
