"""
    Blockdaemon REST API

    Blockdaemon REST API provides a RESTful and uniform way to access blockchain resources, with a rich and reusable model across multiple protocols/cryptocurrencies.  [Documentation](https://docs.blockdaemon.com/reference/rest-api-overview)  ### Currently supported protocols:  * algorand   * mainnet * avalanche    * mainnet-c/testnet-c * bitcoin   * mainnet/testnet * bitcoincash   * mainnet/testnet * dogecoin   * mainnet/testnet * ethereum   * mainnet/holesky/sepolia * fantom   * mainnet/testnet * litecoin   * mainnet/testnet * near   * mainnet/testnet * optimism   * mainnet * polkadot   * mainnet/westend * polygon   * mainnet/amoy * solana   * mainnet/testnet * stellar   * mainnet/testnet * tezos   * mainnet * tron   * mainnet/nile * xrp   * mainnet  ### Pagination Certain resources contain a lot of data, more than what's practical to return for a single request. With the help of pagination, the data is split across multiple responses. Each response returns a subset of the items requested, and a continuation token.  To get the next batch of items, copy the returned continuation token to the continuation query parameter and repeat the request with the new URL. In case no continuation token is returned, there is no more data available.   # noqa: E501

    The version of the OpenAPI document: 3.0.0
    Contact: support@blockdaemon.com
    Generated by: https://openapi-generator.tech
"""


import re  # noqa: F401
import sys  # noqa: F401

from ubiquity.ubiquity_openapi_client.api_client import ApiClient, Endpoint as _Endpoint
from ubiquity.ubiquity_openapi_client.model_utils import (  # noqa: F401
    check_allowed_values,
    check_validations,
    date,
    datetime,
    file_type,
    none_type,
    validate_and_convert_types
)
from ubiquity.ubiquity_openapi_client.model.error import Error
from ubiquity.ubiquity_openapi_client.model.inline_response200 import InlineResponse200


class FeeEstimatorApi(object):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

        def __get_fee_estimate(
            self,
            protocol="bitcoin",
            network="mainnet",
            **kwargs
        ):
            """Get the Fee Estimation  # noqa: E501

            Returns fee estimation in decimals.This endpoint will return 3 fee estimations: fast, medium and slow.   # noqa: E501
            This method makes a synchronous HTTP request by default. To make an
            asynchronous HTTP request, please pass async_req=True

            >>> thread = api.get_fee_estimate(protocol="bitcoin", network="mainnet", async_req=True)
            >>> result = thread.get()

            Args:
                protocol (str): Protocol handle, one of: `avalanche`, `bitcoin`, `bitcoincash`, `ethereum`, `fantom`, `litecoin`, `polkadot`, `polygon` and `solana`. . defaults to "bitcoin", must be one of ["bitcoin"]
                network (str): Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}.. defaults to "mainnet", must be one of ["mainnet"]

            Keyword Args:
                _return_http_data_only (bool): response data without head status
                    code and headers. Default is True.
                _preload_content (bool): if False, the urllib3.HTTPResponse object
                    will be returned without reading/decoding response data.
                    Default is True.
                _request_timeout (int/float/tuple): timeout setting for this request. If
                    one number provided, it will be total request timeout. It can also
                    be a pair (tuple) of (connection, read) timeouts.
                    Default is None.
                _check_input_type (bool): specifies if type checking
                    should be done one the data sent to the server.
                    Default is True.
                _check_return_type (bool): specifies if type checking
                    should be done one the data received from the server.
                    Default is True.
                _host_index (int/None): specifies the index of the server
                    that we want to use.
                    Default is read from the configuration.
                async_req (bool): execute request asynchronously

            Returns:
                InlineResponse200
                    If the method is called asynchronously, returns the request
                    thread.
            """
            kwargs['async_req'] = kwargs.get(
                'async_req', False
            )
            kwargs['_return_http_data_only'] = kwargs.get(
                '_return_http_data_only', True
            )
            kwargs['_preload_content'] = kwargs.get(
                '_preload_content', True
            )
            kwargs['_request_timeout'] = kwargs.get(
                '_request_timeout', None
            )
            kwargs['_check_input_type'] = kwargs.get(
                '_check_input_type', True
            )
            kwargs['_check_return_type'] = kwargs.get(
                '_check_return_type', True
            )
            kwargs['_host_index'] = kwargs.get('_host_index')
            kwargs['protocol'] = \
                protocol
            kwargs['network'] = \
                network
            return self.call_with_http_info(**kwargs)

        self.get_fee_estimate = _Endpoint(
            settings={
                'response_type': (InlineResponse200,),
                'auth': [
                    'apiKeyAuthHeader',
                    'bearerAuth'
                ],
                'endpoint_path': '/{protocol}/{network}/tx/estimate_fee',
                'operation_id': 'get_fee_estimate',
                'http_method': 'GET',
                'servers': None,
            },
            params_map={
                'all': [
                    'protocol',
                    'network',
                ],
                'required': [
                    'protocol',
                    'network',
                ],
                'nullable': [
                ],
                'enum': [
                ],
                'validation': [
                ]
            },
            root_map={
                'validations': {
                },
                'allowed_values': {
                },
                'openapi_types': {
                    'protocol':
                        (str,),
                    'network':
                        (str,),
                },
                'attribute_map': {
                    'protocol': 'protocol',
                    'network': 'network',
                },
                'location_map': {
                    'protocol': 'path',
                    'network': 'path',
                },
                'collection_format_map': {
                }
            },
            headers_map={
                'accept': [
                    'application/json'
                ],
                'content_type': [],
            },
            api_client=api_client,
            callable=__get_fee_estimate
        )
